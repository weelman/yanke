<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Helpful;
use Input;

class HelpfulController extends Controller
{
     public function index()
    {
        $help_list = Helpful::all();
        return view('site.helpful')->with('help_list', $help_list);
    }
    
    public function viewHelp()
    {
        $help_list = Helpful::all();
        return view('lk.helpful')->with('help_list', $help_list);
    }
    
    public function addHelp()
    {
        $help_item = Helpful::all();
        return view('lk.addhelp')->with('help_item', $help_item);
    }
    
    public function postNew()
    {
        $help_item = new Helpful([
			'title' => Input::get('title'),
			'body' => Input::get('body')
		]);	
		$help_item->save();
		return \Redirect::route('helpfulview');   
    }
    public function postEdit()
    {
        $helpid = Input::get('helpitemid');
		
        $help_item = Helpful::findOrFail($helpid);
		
        $help_item->title = Input::get('title');
		$help_item->body = Input::get('body');
		
		$help_item->save();
		
		return \Redirect::route('helpfulview'); 
    }
    public function postDelete($id)
    {
        $help_item = Helpful::findOrFail($id);
		$help_item->delete();
		return \Redirect::route('helpfulview');
    }
}
