<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Message;
use Log;
use Input;
use DB;

class GuestbookController extends Controller
{
    public function index() 
    {
        $message = Message::all();
        $message = Message::orderBy('created_at', 'desc')->paginate(5);

        return view('guestbook')->with('message', $message);
    }
    public function edit($id)
    {
        return view('editmessage');
    }
    public function postNew()
    {
        $message = new Message([
			'username' => Input::get('username'),
			'email' => Input::get('email'),
            'message' => Input::get('message')
		]);
		
		$message->save();
		
		return \Redirect::route('guestbook'); 
    }
    
    public function viewMsg() {
        $messages = Message::all();
        return view('lk.guest')->with('messages', $messages);
    }
    
    public function postDelete($id)
    {
        $messages = Message::findOrFail($id);
		$messages->delete();
		return \Redirect::route('guest');
    }
}
