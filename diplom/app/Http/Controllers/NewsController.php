<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Newsitem;
use Input;

class NewsController extends Controller
{
    public function index()
    {
        $news_list = Newsitem::all();
        return view('site.news')->with('news_list', $news_list);
    }
    
    public function viewNews()
    {
        $news_list = Newsitem::all();
        return view('lk.news')->with('news_list', $news_list);
    }
    public function addNews()
    {
        $news_item = Newsitem::all();
        return view('lk.addnews')->with('news_item', $news_item);
    }
    public function postNew()
    {
        $news_item = new Newsitem([
			'title' => Input::get('title'),
			'body' => Input::get('body')
		]);	
		$news_item->save();
		return \Redirect::route('newsview');   
    }
    public function postEdit()
    {
        $newsid = Input::get('newsitemid');
		
        $news_item = Newsitem::findOrFail($newsid);
		
        $news_item->title = Input::get('title');
		$news_item->body = Input::get('body');
		
		$news_item->save();
		
		return \Redirect::route('newsview'); 
    }
    public function postDelete($id)
    {
        $news_item = Newsitem::findOrFail($id);
		$news_item->delete();
		return \Redirect::route('newsview');
    }
}
