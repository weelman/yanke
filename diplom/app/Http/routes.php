﻿<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return Redirect::to('/');
});
Route::get('/',  function () {
	return view('site.index');
});
Route::get('about', ['as' => 'about', function() {
    return view('site.about');
}]);
Route::get('teachers', ['as' => 'teachers', function() {
    return view('site.teachers');
}]);
Route::get('food', ['as' => 'food', function () {
    return view('site.food');
}]);
Route::get('documents', ['as' => 'documents', function () {
    return view('site.documents');
}]);
Route::get('feedback', ['as' => 'feedback', function () {
    return view('site.feedback');
}]);
Route::get('contacts', ['as' => 'contacts', function () {
    return view('site.contacts');
}]);
Route::get('excursion', ['as' => 'excursion', function () {
    return view('site.excursion');
}]);
Route::get('gallery', ['as' => 'gallery', function () {
    return view('site.gallery');
}]);
Route::get('structure', ['as' => 'structure', function () {
    return view('site.structure');
}]);
Route::get('standart', ['as' => 'standart', function () {
    return view('site.standart');
}]);
Route::get('technical', ['as' => 'technical', function () {
    return view('site.technical');
}]);
Route::get('economic', ['as' => 'economic', function () {
    return view('site.economic');
}]);
Route::get('vacancies', ['as' => 'vacancies', function () {
    return view('site.vacancies');
}]);
Route::get('regime', ['as' => 'regime', function () {
    return view('site.regime');
}]);
Route::get('adaptation', ['as' => 'adaptation', function () {
    return view('site.adaptation');
}]);
Route::get('helpful', ['as' => 'helpful', 'uses' => 'HelpfulController@index']);


Route::get('helpful/{id}', function ($id) {
	$srv = App\Helpful::find($id);
	return view('site.helpfulview')->with('help_item', $srv);
});

Route::get('news', ['as' => 'news', 'uses' => 'NewsController@index']);

Route::get('news/{id}', function ($id) {
	$srv = App\Newsitem::find($id);
	return view('site.newsview')->with('news_item', $srv);
});


Route::get('guestbook', ['as' => 'guestbook', 'uses' => 'GuestbookController@index']);
Route::post('message/add', ['as' => 'messageadd', 'uses'=>'GuestbookController@postNew']);

Route::group(['prefix'=>'lk', 'middleware'=>'auth'],
    function () {
        Route::get('news', ['as' => 'newsview', 'uses' => 'NewsController@viewNews']);
        Route::get('addnews', ['as' => 'addnews', 'uses' => 'NewsController@addNews']);
        Route::post('news/add', ['as'=> 'newsadd', 'uses'=>'NewsController@postNew']);
        Route::get('news/edit/{id}',['as' => 'editnews', function($id) {
            $news_item = App\Newsitem::find($id);
            return view('lk.editnews')->with('news_item', $news_item);
        }]);
        Route::post('news/edit', ['as'=> 'newsedit', 'uses'=>'NewsController@postEdit']);
        Route::get('news/delete/{id}', 'NewsController@postDelete');
        
        Route::get('helpful', ['as' => 'helpfulview', 'uses' => 'HelpfulController@viewHelp']);
        Route::get('addhelp', ['as' => 'addhelp', 'uses' => 'HelpfulController@addHelp']);
        Route::post('helpful/add', ['as'=> 'helpadd', 'uses'=>'HelpfulController@postNew']);
        Route::get('help/edit/{id}',['as' => 'editnews', function($id) {
            $help_item = App\Helpful::find($id);
            return view('lk.edithelp')->with('help_item', $help_item);
        }]);
        Route::post('help/edit', ['as'=> 'helpedit', 'uses'=>'HelpfulController@postEdit']);
        Route::get('help/delete/{id}', 'HelpfulController@postDelete');
        
        Route::get('guest', ['as' => 'guest', 'uses' => 'GuestbookController@viewMsg']);
        Route::get('guest/delete/{id}', 'GuestbookController@postDelete');
    });

Route::auth();
Route::get('/home', 'HomeController@index');
