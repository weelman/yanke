<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Helpful extends Model
{
	use SoftDeletes;
	
    protected $table = 'helpful';
    
	protected $dates = ['deleted_at'];
	
	protected $fillable = ['title', 'body', 'pic', 'htmlbody'];
	
	protected $hidden = ['htmlbody'];
	
	public static $rules = [
		'title' => 'required|max:100',
		'pix' => 'image'
	];	
}
