<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class message extends Model
{
    protected $table = 'message';
    
    protected $fillable = ['username', 'email', 'message'];
    
//    public function getCreatedAtAttribute($date)
//    {
//        $this->attributes['created_at'] = Carbon::createFrom('Y-m-d H:i:s', $date)->format('H:i:s / d.m.Y');
//    }
}
