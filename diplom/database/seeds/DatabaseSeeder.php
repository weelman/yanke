<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         DB::table('users')->delete();
        $this->call(UsersTableSeeder::class);
        DB::table('newsitem')->delete();
        $this->call(NewsTableSeeder::class);
        DB::table('helpful')->delete();
        $this->call(HelpfulTableSeeder::class);
    }
}
