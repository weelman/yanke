<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Newsitem;

class NewsTableSeeder extends Seeder
{
    public function run()
    {
		Newsitem::create([
			'created_at' => '2015-11-05 04:00:01',
			'title' => 'Добрый вечер, уважаемые родители маленьких "нерпят"!',
			'body' => 'Закончились шумные праздники, и жизнь наших малышей опять протекает в спокойном и размеренном ритме. Ребятишки продолжают познавать окружающий мир , наблюдают за происходящими в природе переменами , отмечают все новое, учатся делать выводы о влиянии этих перемен на нашу жизнь. А еще, все более совершенствуя навыки работы с бумагой, красками, клеем, ножницами, пластилином, тестом - стараются поделиться самыми яркими впечатлениями со своими родными и близкими. Посмотрите, каким светлым, добрым, красочным и разнообразным видят мир маленькие труженики!'
			//'pic' => '',
		]);


		Newsitem::create([
			'created_at' => '2015-11-06 04:00:01',
			'title' => '23 февраля!',
			'body' => 'Сегодня каждый в России отмечает главный мужской праздник - 23 февраля. В нашей стране вот уже много лет День Защитника Отечества – мирный праздник, хоть и с военной историей. Праздник мужчин, юношей, мальчишек. Праздник отцов, мужей, сыновей и братьев. Еще один замечательный повод сказать им теплые слова и высказать самые наилучшие пожелания. И мы тоже хотим сегодня поздравить наших дорогих мужчин с праздником !
             С праздником мужества, с праздником чести,
             С праздником силы поздравить хотим.
             Будьте вы сильными, будьте вы смелыми,
             Праздник февральский — для вас, для мужчин!
            
             Будьте опорой всегда и поддержкой
             Женам своим, матерям и сестре.
             Пусть никакие невзгоды, несчастья
             Не повстречаются в жизни нигде!
            
             Спасибо Всем, кто жизнь отдал,
             За Русь родную, за свободу,
             Кто страх забыл и воевал,
             Служа любимому народу.
            
             Спасибо Вам,
             Ваш подвиг вечен,
             Пока жива моя страна,
             Вы в душах наших,
             В нашем сердце,
             Героев не забыть нам никогда!
            
             Участники праздника собираются в назначенное время. Спасибо выпускникам-"нерпятам" - Насте Б., Полинке Г., Сашульке С. и Анюте Г. , которые откликнулись на наше приглашение и приняли участие в празднике!',
			'htmlbody' => ''
			//'pic' => '',
		]);

		Newsitem::create([
			'created_at' => '2015-11-07 04:00:01',
			'title' => 'Подготовка к 23 февраля',
			'body' => 'Добрый вечер, уважаемы родители маленьких " нерпят"! Добрый вечер, уважаемые гости нашей страницы! Сегодня мы хотим показать Вам, как мы готовились к празднику - Дню настоящих мужчин 23 февраля. Пластилинография - замечательная техника рисования! Нашим ребятам она очень полюбилась. А так мы готовили наши подарки для пап. Портреты, изготовленные детьми совместно со взрослыми - это первый вклад малышей в семейные портретные галереи ! И про отдых мы тоже не забывали! Ждем всех, кто хочет весело овладевать новыми навыками и умениями, кто любит петь, танцевать и просто радоваться каждому новому дню в нашу дружную компанию. На все вопросы ответим по тлф. 98-21-71. С уважением - Марина. ',
			'htmlbody' => ''
			//'pic' => '',
		]);
        
        Newsitem::create([
			'created_at' => '2015-11-07 04:00:01',
			'title' => '8 марта',
			'body' => 'Дорогие наши, милые, нежные, прекрасные женщины! Поздравляем Вас с  самым главным женским праздником, олицетворяющим пробуждение всей природы, и мужчин от зимнего сна. Пусть этот праздник – 8 марта – будет для всех вас тем днем, когда все вокруг будут радоваться лишь одному вашему появлению, и пусть вас окружают лишь  одни ослепительные улыбки. Пусть этот день, 8 Марта, подарит вам прекрасное настроение, исполнение всех сокровенных желаний! Пусть он станет стартом для новых начинаний, которые принесут только радость! Счастья вам и весеннего настроения не только в этот замечательный праздник, но и во все оставшиеся 364 дня в году.Искренних вам комплиментов и добрых слов всегда! С весенним праздником! С уважением  - коллектив "Нерпенка"! 
            И в подарок - несколько фотографий с нашего праздника!',
			'htmlbody' => ''
			//'pic' => '',
		]);
        Newsitem::create([
			'created_at' => '2015-11-07 04:00:01',
			'title' => 'Как мы готовились поздравлять наших мам',
			'body' => 'Добрый день, уважаемые родители маленьких "нерпят"! Добрый день, уважаемые гости нашей страницы! Сегодня свой краткий фоторепортаж мы посвятим теме "Как мы готовились поздравлять наших мам". Свои подарки мы начали готовить за 2 недели до наступления праздника. Работу мы запланировали большую, поэтому разделили ее на несколько этапов. Первый этап - изготовление поздравительных открыток. Для этого старшие ребята затонировали листы бумаги в яркие , радостные цвета и нарисовали на них букетики мимозы. Чтобы работать было веселее, мы использовали в работе одноразовые вилки, ватные палочки, бумажные салфетки . В завершение работы - приклеили вазы , вырезанные из бумаги и украшенные декоративными элементами. А малыши изготовили открытки с тюльпанами в технике рисования ладошками.',
			'htmlbody' => ''
			//'pic' => '',
		]);
	}
}
