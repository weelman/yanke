<div class="wrapper" style="height: 0px;">

    <div class="inner-wrapper">
        <div class="messages">@foreach ($message as $messageitem)
            <div class="panel panel-default">
                <div class="panel-heading" style="border-top-left-radius: 13px; border-top-right-radius: 13px;">
                    <h3 class="panel-title">
                <span>#{!! $messageitem->id !!}
                @unless (empty($messageitem->email))
                   <a href="mailto:{{ $messageitem->email}}">{{ $messageitem->username }}</a>
                @else
                   {{ $messageitem->username }}
                @endunless
                </span>
                <span class="pull-right label label-info">
                    {{ $messageitem->created_at }}
                </span>
            </h3>
                </div>

                <div class="panel-body">
                    {{ $messageitem->message }}
                </div>

            </div>@endforeach
            <div class="text-center">
                {!! $message->render() !!}
            </div>
        </div>
    </div>
</div>