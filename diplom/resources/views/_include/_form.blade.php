<div class="wrapper" style="height: 430px;">
    <div class="blur"></div>
    <div class="inner-wrapper">
        <h3>Гостевая книга</h3> {!! Form::open(array('url' => URL::route('messageadd'), 'class'=>'form', 'id'=>'form')) !!}
        <div class="col-sm-12">
            <div class="form-group col-sm-6 col-sm-offset-3">
                <label for="username"><h4>Имя: *</h4></label>
                <input class="form-control" placeholder="Имя" name="username" type="text" id="username">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group col-sm-6 col-sm-offset-3">
            <label for="email"><h4>E-mail:</h4></label>
            <input class="form-control" placeholder="E-mail" name="email" type="email" id="email">
        </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group col-sm-6 col-sm-offset-3">
            <label for="message">Сообщение: *</label>
            <textarea class="form-control" rows="5" placeholder="Текст сообщения" name="message" cols="50" id="message"></textarea>
            </div>
        </div>

        <div class="form-group">
<!--            <input type="submit" class="btn btn-primary" value="добавить">-->
           <button type="submit" class="btn btn-success">Отправить</button>
        </div>
        <!--        </form>-->
        {!! Form::close() !!}
    </div>
</div>