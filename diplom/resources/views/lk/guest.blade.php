@extends('layouts.app') @section('content')
@if (is_null($messages))
    <h1 align="center">Записи в гостевой книге не найдены</h1>
@else
<h2><p class="bg-info text-center">Гостевая книга</p></h2>
<table class="table table-stripped">
    <thead>
        <tr>
            <th>Имя</th>
            <th>email</th>
            <th>Сообщение</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
       
        
        @foreach ($messages as $message)
        <tr>
            <td><strong>{{ $message->username }}</strong></td>
            <td>{{ $message->email }}</td>
            <td>{{ str_limit($message->message, 100) }}</td>
            <td>
                <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $message->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            </td>
        </tr>
        <div class="modal fade bs-example-modal-sm-{{ $message->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                        <a href='guest/delete/{{ $message->id }}'><button type="button" class="btn btn-success">Да</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach 
        
    </tbody>
</table>

@endif
@endsection