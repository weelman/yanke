@extends('layouts.app') @section('content')


        {!! Form::open(array('url' => URL::route('helpedit'), 'class'=>'form')) !!}
        {{ Form::hidden('helpitemid', $help_item->id) }}
        <div class="panel panel-default">
            <div class="panel-heading">Добавить новость</div>
            <div class="panel-body">
                <div class="form-group col-sm-12">
                    {{ Form::label('title', 'Название', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('title', $help_item->title, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('body', 'Описание', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('body', $help_item->body, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::submit('Сохранить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!}


@endsection
