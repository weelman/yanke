@extends('layouts.app') @section('content')


        {!! Form::open(array('url' => URL::route('helpadd'), 'class'=>'form')) !!}
        <div class="panel panel-default">
            <div class="panel-heading">Добавить новость</div>
            <div class="panel-body">
                <div class="form-group col-sm-12">
                    {{ Form::label('title', 'Название', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('title', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::label('body', 'Описание', array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                    {{ Form::text('body', null, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    {{ Form::submit('Добавить', array('class'=>'btn btn-success col-sm-offset-10 col-sm-2')) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!}


@endsection
