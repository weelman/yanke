@extends('layouts.app') @section('content')
<h2><p class="bg-info text-center">Полезные статьи</p></h2>
<table class="table table-stripped">
    <thead>
        <tr>
            <th>Название</th>
            <th>Описание</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($help_list as $help_item)
        <tr>
            <td><strong>{{ $help_item->title }}</strong></td>
            <td>{{ str_limit($help_item->body, 100) }}</td>
            <td>
                <a href='help/edit/{{ $help_item->id }}' class='btn btn-default'><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                <button class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $help_item->id }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            </td>
        </tr>
        <div class="modal fade bs-example-modal-sm-{{ $help_item->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить эту запись?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                        <a href='help/delete/{{ $help_item->id }}'><button type="button" class="btn btn-success">Да</button></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach 
    </tbody>
</table>
<a href='{{ URL::route('addhelp') }}' class='btn btn-success'>Добавить статью</a>

@endsection