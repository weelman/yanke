﻿<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Главная</title>
    <!--
    
    <link href="css/testdesign.css" rel="stylesheet">
-->
    <!--    <link href="css/design.css" rel="stylesheet">-->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/news.css" rel="stylesheet">
    <link href="/css/testdesign.css" rel="stylesheet">
</head>

<body>
    <script src="/js/jquery-2.2.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <header>
        <img src="/img/nerp.png" class="nerp-img" />
    </header>
    <div class="container">
        <ul id="nav">
            <li><a href="/">Главная</a>
                <ul>
                    <li><a href="{{ URL::route('guestbook') }}">Гостевая книга</a></li>
                    <li><a href="{{ URL::route('contacts') }}">Контакты</a></li>
                    <li><a href="/home">Авторизация</a></li>
                </ul>
            </li>
            <li><a href="{{ URL::route('about') }}">Сведения о ДОУ</a>
                <ul>
                    <li><a href="{{ URL::route('about') }}">Основные сведения</a></li>
                    <li><a href="{{ URL::route('teachers') }}">Педагогический состав</a></li>
                    <li><a href="{{ URL::route('structure') }}">Структура и органы управления ОО</a></li>
                    <li><a href="{{ URL::route('documents') }}">Документы</a></li>
                    <li><a href="{{ URL::route('standart') }}">Образовательные стандарты</a></li>
                    <li><a href="{{ URL::route('technical') }}">Материально-техническое обеспечение</a></li>
                  
                    <li><a href="{{ URL::route('vacancies') }}">Вакансии</a></li>

                </ul>
                <div class="clear"></div>
            </li>
            <li><a href="{{ URL::route('food') }}">Условия для детей</a>
                <ul>
                    <li><a href="{{ URL::route('food') }}">Питание</a></li>
                    <li><a href="{{ URL::route('regime') }}">Режим дня</a></li>
                    <li><a href="{{ URL::route('adaptation') }}">Адаптация</a></li>
                    <li><a href="{{ URL::route('excursion') }}">Экскурсия</a></li>
                </ul>
            </li>
            <li><a href="{{ URL::route('gallery') }}">Галерея</a></li>
            <li><a href="{{ URL::route('helpful') }}">Полезные статьи</a></li>
            <li><a href="{{ URL::route('news') }}">Новости</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="container">
        @yield('sidecontent')
    </div>
    <script>
        $(document).ready(function () {

            $('#nav li').hover(
                function () {
                    //show its submenu
                    $('ul', this).stop().slideDown(100);

                },
                function () {
                    //hide its submenu
                    $('ul', this).stop().slideUp(100);
                }
            );

        });
    </script>
</body>

</html>