@extends('mainlayout') @section('sidecontent')

<div class="wrapper">
        <div class="blur" style="height: 300px;"></div>
        <div class="inner-wrapper">

<h3><a href="/pdf/fgos.pdf" style="color: #fbf">Федеральный государственный образовательный стандарт дошкольного образования</a></h3>
        <h3><a href="/doc/obrazec_zapolnenija_dogovora_2012.doc" style="color: #fbf">Образец заполнения договора</a></h3>
        <h3><a href="/pdf/PR_1155.pdf" style="color: #fbf">План - график введения ФГОС в дошкольном учреждении</a></h3>
        <h3><a href="/pdf/Metodicheskie-rekomendacii_finansy_DO.pdf" style="color: #fbf">Методические рекомендации по реализации полномочий органов гос.власти субъектов РФ</a></h3>
       
    </div>
</div>

@endsection