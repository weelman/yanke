@extends('mainlayout') @section('sidecontent')

<div class="wrapper">
    <div class="blur" style="height: 400px;"></div>
    <div class="inner-wrapper">
        <h2>Образование. Образовательные стандарты</h2>
        <h3><a href="/pdf/fgos.pdf" style="color: #fbf">Федеральный государственный образовательный стандарт дошкольного образования</a></h3>
        <h3><a href="/pdf/kommentarii_k_fgos_doshkolnogo_obrazovanija.pdf" style="color: #fbf">Комментарии к ФГОС дошкольного образования</a></h3>
        <h3><a href="/pdf/plan_grafik.pdf" style="color: #fbf">План - график введения ФГОС в дошкольном учреждении</a></h3>
        <h3><a href="/pdf/polozhenie_o_rabochej_gruppe_po_vvedeniju_fgos.pdf" style="color: #fbf">Положение о рабочей группе по введению ФГОС ДО</a></h3>
        <h3><a href="/pdf/otchet_po_vvedeniju_fgos_mdou_79.pdf" style="color: #fbf">Отчет о введении ФГОС ДО в дошкольном учреждении за 2014 год</a></h3>
        <h3><a href="/pdf/sobranie_fgos_2015.pdf" style="color: #fbf">ФГОС для родителей (родительское собрание 2015 год)</a></h3>
    </div>
</div>

@endsection