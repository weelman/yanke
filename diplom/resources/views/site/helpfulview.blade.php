@extends('mainlayout') @section('sidecontent')

    <div class="wrapper">
        <div class="blur"></div>
        <div class="inner-wrapper">
            <h2>{{ $help_item->title }}</h2>
            <h3>{{ $help_item->body }}</h3>
        </div>
    </div>

@endsection