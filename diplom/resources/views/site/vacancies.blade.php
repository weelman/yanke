@extends('mainlayout') @section('sidecontent')

<div class="wrapper">
    <div class="blur" style="height: 200px;"></div>
    <div class="inner-wrapper">
     
 <table class="table table-stripped" style="color:#ccffff; 20px;">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Возрастная группа</th>
                    <th>Воспитатели группы</th>
                    <th>Количество вакантных мест</th>
                </tr>
            </thead>
            <tbody style="text-align: left;">
        <tr>
                    <td><strong></strong></td>
                    <td>II  младшая группа(дети от 3 до 4 лет)</td>
                    <td>Шнитуленко Т.Н. Кайнова Г.Н.</td>
                    <td>Нет</td>
                </tr>
 	<tr>
                    <td><strong></strong></td>
                    <td>Средняя группа «А» (дети от 4 до 5 лет)</td>
                    <td>Федяева Г.В. Позолотина О.А.</td>
                    <td>Нет</td>
                </tr>
	<tr>
                    <td><strong></strong></td>
                    <td>Старшая группа (дети от 5 до 6 лет)</td>
                    <td>Федяева Г.В. Позолотина О.А.</td>
                    <td>Нет</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection