@extends('mainlayout') @section('sidecontent')

<div class="wrapper">
    <div class="blur"></div>
    <div class="inner-wrapper">


        <h2> <b> <span style="color: #C2CC45; text-shadow: 2px 2px 5px black;">     
 Каждое утро маленьких воспитанников уютом и теплом встречают взрослые, 
которым интересны дети! Просторные и светлые, наполненные заботой воспитателей, 
детей ждут групповые комнаты для занятий и сна. Комфортные бытовые условия, 
современный учебный материал, игрушки и развивающие игры и, конечно, 
тёплая домашняя обстановка – основа развивающей среды нашего детского сада. 
  
    </span></b> </h2>

        <h2> Групповые комнаты</h2>
        <div id="myCarousel1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel1" data-slide-to="1"></li>
                <li data-target="#myCarousel1" data-slide-to="2"></li>
                <li data-target="#myCarousel1" data-slide-to="3"></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="img/2.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item">
                    <img src="img/9.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item">
                    <img src="img/3.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>


                <div class="item">
                    <img src="img/4.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
</div>
<div class="wrapper">
    <div class="blur"></div>
    <div class="inner-wrapper">


        <h2> Спальни</h2>


        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>


            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="img/2.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item">
                    <img src="img/10.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item">
                    <img src="img/12.jpg">
                    <div class="carousel-caption">
                    </div>
                </div>


            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>




@endsection